
from bs4 import BeautifulSoup as bs
import requests
from urllib.request import urlretrieve, urlopen
import os
from lxml import etree
import lxml
import json
from src.utils.retry import retry
from src.product import product_sephora
import logging
from src.utils.log_config import logger

product_links = {}

# stuffs you need to know
protocol = 'https://'
site = 'www.sephora.com'

# variables used for moisturizers page
moisturizer_page = '/shop/moisturizer-skincare'
moisturizer_page_filter = '?currentPage='

def get_moisturizers_OLD(request_url):
    #create session
    session = requests.session()

    #get page
    response = session.get(request_url)

    #convert to soup for filtering
    tree = bs(response.content, 'lxml')

    product_grid = tree.select('div[data-comp="ProductGrid"]')[0]

    prod_groups = product_grid.findChildren('div', recursive=False)

    cntr = 0

    for group in prod_groups:
        products = group.findChildren('div', recursive=False)
        for product in products:
            cntr += 1
            print(str(cntr))
            prod_details = product.findChildren('a')[0]
            prod_link = prod_details['href']

            prod_name = prod_details.select('div[data-comp="ProductDisplayName"]')[0]
            prod_name_text = prod_name.findAll('span')[1].text

        product_links[prod_name_text] = prod_link

    print(product_links)

def get_soup(url):

    response = requests.get(url)
    soup = bs(response.content, "html.parser")

    return soup

def get_page_moisturizers(soup):

    page_json_elem = soup.find('script',
                               attrs={
                                   'data-comp': 'PageJSON'})

    # load str into obj
    page_json = json.loads(page_json_elem.text)

    # find CatalogPage element
    for x in page_json:
        if x['class'] == 'CatalogPage':
            catalog = x
            break

    # get list of products
    products = catalog['props']['products']

    return products

if __name__ == '__main__':

    logger.info('STARTING SCRAPER...')

    #loop through pages and get products.
    page_num = 1
    while True:

        request_url = protocol + site + moisturizer_page + moisturizer_page_filter + str(page_num)

        logger.info('retrieving soup...')

        m_soup = get_soup(request_url)

        logger.info('obtained soup, processing page, getting products...')

        m_products = get_page_moisturizers(m_soup)

        #check for products. if not, then no more pages to loop through.
        if m_products == []:

            if page_num == 1:
                logger.warning('There are no pages to process for scraper.')
                break

            logger.info('Scraping complete, No more pages to process.')
            break

        for product in m_products:
            ps = product_sephora(product)
            ps.get_product_details()
            data_to_load = ps.p_obj

        print(m_products)



