import logging
import logging.config
from datetime import datetime

def logging_config():

    dt_start = datetime.now()
    str_start = dt_start.strftime('%Y%m%d_%H%M%S')

    logging_config = dict(
        version=1,
        formatters={
            'f': {
                'format': '%(asctime)s-%(name)s-%(levelname)s: %(message)s'}
        },
        handlers={
            'sh': {
                'class': 'logging.StreamHandler',
                'formatter': 'f',
                'level': logging.INFO
            },
            'fh_debug': {
                'class': 'logging.handlers.RotatingFileHandler',
                'formatter': 'f',
                'level': logging.DEBUG,
                "filename": "logs/scraper_debug_" + str_start + ".log",
                "maxBytes": 10485760,
                "backupCount": 20,
                "encoding": "utf8"
            },
            'fh_info': {
                'class': 'logging.handlers.RotatingFileHandler',
                'formatter': 'f',
                'level': logging.INFO,
                "filename": "logs/scraper_info_" + str_start + ".log",
                "maxBytes": 10485760,
                "backupCount": 20,
                "encoding": "utf8"
            },
            'fh_warning': {
                'class': 'logging.handlers.RotatingFileHandler',
                'formatter': 'f',
                'level': logging.WARNING,
                "filename": "logs/scraper_warning_" + str_start + ".log",
                "maxBytes": 10485760,
                "backupCount": 20,
                "encoding": "utf8"
            }
        },
        root={
            # 'handlers': ['sh'],
            'handlers': ['sh', 'fh_debug', 'fh_info', 'fh_warning'],
            'level': logging.DEBUG,
        },
    )

    return logging_config

logging.config.dictConfig(logging_config())

logger = logging.getLogger('SEPHORA SCRAPER')

