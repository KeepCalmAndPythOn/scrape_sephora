import json
import requests
from bs4 import BeautifulSoup as bs
from src.utils.log_config import logger
from nested_lookup import nested_lookup
import re

class product_sephora():

    """
    Class for retrieving and creating product details json for sephora products.

    URL example:
    https://www.sephora.com/product/a-passioni-retinol-cream-P439926'

    input: dictionary:
            {'brandName': 'Drunk Elephant',
            'currentSku':
                {'biExclusiveLevel': 'none',
                   'imageAltText': 'Drunk Elephant - A-Passioni™ Retinol Cream',
                   'isAppExclusive': False,
                   'isBI': False,
                   'isBest': False,
                   'isFirstAccess': False,
                   'isLimitedEdition': False,
                   'isLimitedTimeOffer': False,
                   'isNatural': False,
                   'isNew': True,
                   'isOnlineOnly': False,
                   'isOrganic': False,
                   'isSephoraExclusive': True,
                   'listPrice': '$74.00',
                   'salePrice': '',
                   'skuId': '2166973',
                   'skuType': 'Standard'},
          'displayName': 'A-Passioni™ Retinol Cream',
          'heroImage': '/productimages/sku/s2166973-main-grid.jpg',
          'image135': '/productimages/sku/s2166973-main-grid.jpg',
          'image250': '/productimages/sku/s2166973-main-hero.jpg',
          'image450': '/productimages/sku/s2166973-main-Lhero.jpg',
          'productId': 'P439926',
          'rating': '4.6154',
          'targetUrl': '/product/a-passioni-retinol-cream-P439926',
          'url': '/v1/catalog/products/P439926?preferedSku=2166973'}

    """

    def __init__(self, product):

        self.protocol = 'https://'
        self.site = 'www.sephora.com'

        self.product_dict = product
        self.product_url = self.create_product_url()
        self._p_obj = {}
        self.regex_strings = {'forConcerns': "(?<=Skincare Concerns: </b>)(.+?)(?=<br>)",
                              'forSkinType': "(?<=Skin Type: </b>)(.+?)(?=<br>)"}

    @classmethod
    def search_text(self, ss,text):

        m = re.search(ss, text)

        return m.group(1)

    def create_product_url(self):

        target_url = self.product_dict['targetUrl']

        product_url = self.protocol + self.site + target_url

        return product_url

    def get_product_details(self):

        #request product url
        response = requests.get(self.product_url)

        #create soup for interrogating
        soup = bs(response.content, "html.parser")

        # get script elem containing pageJSON
        page_json_elem = soup.find('script',
                                   attrs={
                                       'data-comp': 'PageJSON'})

        # load str into obj
        page_json = json.loads(page_json_elem.text)

        self.product_details = page_json[7]['props']

    def getPobj(self):

        if self._p_obj:

            return self._p_obj

        else:

            long_descrip = nested_lookup("longDescription", self.product_details)

            forConcerns = self.search_text(self.regex_strings['forConcerns'], long_descrip[1])

            forSkinType = self.search_text(self.regex_strings['forSkinType'], long_descrip[1])

            self._p_obj = {'name': self.product_dict['currentSku']['imageAltText'],
                     'brand': self.product_dict['brandName'],
                     'listPrice': self.product_dict['currentSku']['listPrice'],
                     'isNatural': self.product_dict['currentSku']['isNatural'],
                     'isOrganic': self.product_dict['currentSku']['isOrganic'],
                     'forConcerns': [],
                     'forSkinType': [],
                     'ingredients': [],
                     'reviews': {}}

    p_obj = property(getPobj)
